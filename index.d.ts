import { StoreWrapper } from "./StoreWrapper";
export declare class Stores {
    static stores: Map<string, StoreWrapper<any>>;
    static getStore<T extends StoreWrapper<any>>(name: string): T | null;
    static addStore<T>(name: string, store: StoreWrapper<T>): Map<string, StoreWrapper<any>> | null;
    static removeStore(name: string): void;
}
export declare const useStore: <T extends StoreWrapper<any>>(storeName: string) => T;
export declare const usePartialStore: <T extends StoreWrapper<any> = any, State = any>(storeName: string, listenTo: (store: State) => any) => T;
