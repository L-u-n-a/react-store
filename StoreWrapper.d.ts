export declare class StoreWrapper<T> {
    listeners: Array<() => void>;
    state: T | undefined;
    constructor(initialState?: T);
    getState(): T | undefined;
    setState(newState: T): void;
    subscribe(listener: () => void): () => void;
    emitChange(): void;
}
