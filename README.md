# Ulti state

An efficient way to manage (global) state in React applications.

## Examples

### Basic example

First, create a store.

```ts
import {StoreWrapper} from "ulti-state/StoreWrapper";

export class Example extends StoreWrapper<number> {
    constructor(initialState?: number) {
        super(initialState ?? 0);
    }

    increment() {
        if(!this.state) return;
        this.setState(this.state + 1);
    }
    
    decrement() {
        if(!this.state) return;
        this.setState(this.state - 1);
    }
}
```

Then, hook it up to the React application.
You don't need to execute this code in a react component or hook. Rather, you may execute it anywhere in your application.
it's perfectly possible to execute it in your main.ts, before your React application is initialised.

```tsx
import {Stores} from "ulti-state";
import Example from "./path-to-example-here";

// Please note how we add the store above the react application, and not inside it.
// You may also declare stores inside your React application, but please make sure to only declare them ones.
Stores.addStore("Example", new Example(5));

ReactDOM.createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
       <App />
    </React.StrictMode>,
)
```

Now you may use it anywhere in your React application as follows.

```tsx
import {useStore} from "ulti-state";

export const ExampleComponent = () => {
    const example = useStore("Example");
    
    return (
        <div>
            <p>Current value: {example.state}</p>
            <button onClick={() => example.increment()}>Increment</button>
            <button onClick={() => example.decrement()}>Decrement</button>
        </div>
    );
}
```

### Advanced example

There is also the option to use a "partial store". This is a store where only part of the state will cause the component listening to it to re-render.

For this, we first create a more complex store.

```ts
import {StoreWrapper} from "ulti-state/StoreWrapper";

export interface ExampleState {
    counter: number;
    name: string;
}

export class Example extends StoreWrapper<ExampleState> {
    constructor(initialState?: ExampleState) {
        super(initialState ?? {counter: 0, name: "John Doe"});
    }

    increment() {
        if(!this.state) return;
        this.setState({...this.state, counter: this.state.counter + 1});
    }
    
    decrement() {
        if(!this.state) return;
        this.setState({...this.state, counter: this.state.counter - 1});
    }
    
    setName(name: string) {
        if(!this.state) return;
        this.setState({...this.state, name});
    }
}
```

Then, hook it up to the React application.
Make sure to run this before any component that uses the store is rendered.

```ts
Stores.addStore("Example", new Example({counter: 5, name: "Jane Doe"}));
```

Now you may use it anywhere in your React application as follows.

```tsx
import {usePartialStore} from "ulti-state";
import {Example, ExampleState} from "./path-to-example-here";

export const Counter = () => {
    const example = usePartialStore<Example, ExampleState>("Example", (state) => state.counter);
    
    return (
        <div>
            <p>Current value: {example.state?.counter}</p>
            <button onClick={() => example.increment()}>Increment</button>
            <button onClick={() => example.decrement()}>Decrement</button>
        </div>
    );
}
```

If the name state changes anywhere else in the application, the component above will not re-render, since it only listens to the "counter" state.

## Update state outside of React

It's also possible to update the state outside of React, or to update the state in a non-React environment, or React component that itself does not subscribe to the store, but does want to update the values in it.

For our simple basic above, it would like this:
```ts
import {Stores} from "ulti-state";
import Example from "./path-to-example-here";

Stores.getStore<Example>("Example")?.increment();
``` 

Any component that now uses the "Example" store, and listens to the changed value, will re-render with the new value.