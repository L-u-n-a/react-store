import { useSyncExternalStore } from "react";
export class Stores {
    static getStore(name) {
        if (!Stores.stores.has(name)) {
            console.error(`Ulti-state -> Store ${name} does not exist`);
            return null;
        }
        return Stores.stores.get(name);
    }
    static addStore(name, store) {
        if (Stores.stores.has(name)) {
            console.error(`Ulti-state -> Store ${name} already exists. To replace it, use removeStore(name) first.`);
            return null;
        }
        return Stores.stores.set(name, store);
    }
    static removeStore(name) {
        if (!Stores.stores.has(name)) {
            throw new Error(`Store with name: ${name} does not exists.`);
        }
        Stores.stores.delete(name);
    }
}
Stores.stores = new Map();
export const useStore = (storeName) => {
    const store = Stores.getStore(storeName);
    if (!store) {
        throw new Error(`Ulti-state -> Store ${storeName} does not exist`);
    }
    useSyncExternalStore((s) => store.subscribe(s), () => store.getState());
    return store;
};
export const usePartialStore = (storeName, listenTo) => {
    const store = Stores.getStore(storeName);
    if (!store) {
        throw new Error(`Ulti-state -> Store ${storeName} does not exist`);
    }
    useSyncExternalStore((s) => store.subscribe(s), () => listenTo(store.state));
    return store;
};
