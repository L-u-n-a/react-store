import {StoreWrapper} from "./StoreWrapper";
import {useSyncExternalStore} from "react";

export class Stores {
  public static stores: Map<string, StoreWrapper<any>> = new Map();

  public static getStore<T extends StoreWrapper<any>>(name: string): T | null {
    if (!Stores.stores.has(name)) {
      console.error(`Ulti-state -> Store ${name} does not exist`);
      return null;
    }

    return Stores.stores.get(name) as T;
  }

  public static addStore<T>(name: string, store: StoreWrapper<T>) {

    if (Stores.stores.has(name)) {
      console.error(`Ulti-state -> Store ${name} already exists. To replace it, use removeStore(name) first.`);
      return null;
    }

    return Stores.stores.set(name, store);
  }

  public static removeStore(name: string) {
    if (!Stores.stores.has(name)) {
      throw new Error(`Store with name: ${name} does not exists.`);
    }

    Stores.stores.delete(name);
  }
}

export const useStore = <T extends StoreWrapper<any>>(storeName: string): T => {
  const store = Stores.getStore<T>(storeName);

  if (!store) {
    throw new Error(`Ulti-state -> Store ${storeName} does not exist`);
  }

  useSyncExternalStore((s) => store.subscribe(s), () => store.getState());
  return store as T;
}

export const usePartialStore = <T extends StoreWrapper<any> = any, State = any>(storeName: string, listenTo: (store: State) => any): T => {
  const store = Stores.getStore<T>(storeName);

  if (!store) {
    throw new Error(`Ulti-state -> Store ${storeName} does not exist`);
  }

  useSyncExternalStore((s) => store.subscribe(s), () => listenTo(store.state));
  return store;
}
