export class StoreWrapper<T> {
  listeners: Array<() => void> = [];
  state: T | undefined = undefined;

  constructor(initialState?: T) {
    this.state = initialState;
  }

  getState(): T | undefined {
    return this.state;
  }

  setState(newState: T): void {
    this.state = newState;
    this.emitChange();
  }

  subscribe(listener: () => void): () => void {
    this.listeners = [...this.listeners, listener];
    return () => {
      this.listeners = this.listeners.filter(l => l !== listener);
    }
  }

  emitChange(): void {
    for (const listener of this.listeners) {
      listener();
    }
  }
}