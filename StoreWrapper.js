export class StoreWrapper {
    constructor(initialState) {
        this.listeners = [];
        this.state = undefined;
        this.state = initialState;
    }
    getState() {
        return this.state;
    }
    setState(newState) {
        this.state = newState;
        this.emitChange();
    }
    subscribe(listener) {
        this.listeners = [...this.listeners, listener];
        return () => {
            this.listeners = this.listeners.filter(l => l !== listener);
        };
    }
    emitChange() {
        for (const listener of this.listeners) {
            listener();
        }
    }
}
